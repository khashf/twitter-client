// Module Dependencies
var http = require('http');
var express = require('express');
var search = require('./search');

// Create app
var app = express();

// Configuration
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.set('view options', {layout: false});

// Routes
app.get('/', function (req, res) {
	res.render('index');
});
app.get('/search', function (req, res, next) {
	search(req.query.q, function (err, tweets) {
		if (err) return next(err);
		res.render('search', { results: tweets, search: req.query.q});
	});
});


console.log(app.set('views'));

// Listen
app.listen(3000);